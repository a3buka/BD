package models

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse and unparse this JSON data, add this code to your project and do:
//
//    empty, err := UnmarshalEmpty(bytes)
//    bytes, err = empty.Marshal()

type IDVacancy struct {
	ID string `json:"id"`
}

type Query struct {
	Query string `json:"query"`
}

type Vacancy struct {
	Id                 string
	Context            string             `json:"@context"`
	Type               string             `json:"@type"`
	DatePosted         string             `json:"datePosted"`
	Title              string             `json:"title"`
	Description        string             `json:"description"`
	Identifier         Identifier         `json:"identifier"`
	ValidThrough       string             `json:"validThrough"`
	HiringOrganization HiringOrganization `json:"hiringOrganization"`
	JobLocation        []JobLocation      `json:"jobLocation"`
}

type HiringOrganization struct {
	Type   string `json:"@type"`
	Name   string `json:"name"`
	Logo   string `json:"logo"`
	SameAs string `json:"sameAs"`
}

type Identifier struct {
	Type  string `json:"@type"`
	Name  string `json:"name"`
	Value string `json:"value"`
}

type JobLocation struct {
	Type    string `json:"@type"`
	Address string `json:"address"`
}
