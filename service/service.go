package service

import (
	"gitlab.com/a3buka/BD/internal/models"
)

type SeleniumService interface {
	Create(name string) error
	GetById(id int) (models.Vacancy, error)
	GetList() ([]models.Vacancy, error)
	Delete(id int) error
}

type VacansyService struct {
	SeleniumService
}

func NewVacansyService(selenium SeleniumService) *VacansyService {
	return &VacansyService{
		SeleniumService: NewService(selenium),
	}
}

type Service struct {
	serv SeleniumService
}

func NewService(servicer SeleniumService) *Service {
	return &Service{serv: servicer}
}

func (s Service) Create(name string) error {
	return s.serv.Create(name)
}

func (s Service) GetById(id int) (models.Vacancy, error) {
	return s.serv.GetById(id)
}

func (s Service) GetList() ([]models.Vacancy, error) {
	return s.serv.GetList()
}

func (s Service) Delete(id int) error {
	return s.serv.Delete(id)
}
