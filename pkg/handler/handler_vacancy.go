package handler

import (
	"encoding/json"
	"net/http"
	"strconv"

	"gitlab.com/a3buka/BD/internal/models"
)

func (h *Handler) List(w http.ResponseWriter, r *http.Request) {
	list, err := h.service.SeleniumService.GetList()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")

	err = json.NewEncoder(w).Encode(list)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (h *Handler) GetId(w http.ResponseWriter, r *http.Request) {
	var id models.IDVacancy
	err := json.NewDecoder(r.Body).Decode(&id)
	num, err := strconv.Atoi(id.ID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	data, err := h.service.SeleniumService.GetById(num)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")

	err = json.NewEncoder(w).Encode(data)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

}

func (h *Handler) DeleteId(w http.ResponseWriter, r *http.Request) {
	var id models.IDVacancy
	err := json.NewDecoder(r.Body).Decode(&id)
	num, err := strconv.Atoi(id.ID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = h.service.SeleniumService.Delete(num)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")

}

func (h *Handler) Search(w http.ResponseWriter, r *http.Request) {
	var name models.Query
	err := json.NewDecoder(r.Body).Decode(&name)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
	}

	err = h.service.SeleniumService.Create(name.Query)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")

	err = json.NewEncoder(w).Encode(err)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

}
