package repository

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/a3buka/BD/internal/models"
	"go.mongodb.org/mongo-driver/mongo"
)

type Selemiumer interface {
	Create(name string) error
	GetById(id int) (models.Vacancy, error)
	GetList() ([]models.Vacancy, error)
	Delete(id int) error
}

type RepositorySelenium struct {
	Selemiumer
}

func NewRepositorySelenium(db *sqlx.DB, client *mongo.Collection) *RepositorySelenium {
	if db != nil {
		return &RepositorySelenium{NewSeleniumSroragePostgres(db)}
	}
	return &RepositorySelenium{NewRepositorySeleniumMongo(client)}
}
