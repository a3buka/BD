package repository

import (
	"errors"
	"github.com/jmoiron/sqlx"
	"gitlab.com/a3buka/BD/internal/models"
	"gitlab.com/a3buka/BD/pkg/selenium"
	"log"
)

type SelenuimStoragePostgres struct {
	db *sqlx.DB
}

func NewSeleniumSroragePostgres(db *sqlx.DB) *SelenuimStoragePostgres {
	return &SelenuimStoragePostgres{
		db: db,
	}
}

const (
	TablePostgres = `INSERT INTO vacancy (
		 id, context, type, datePosted, tittle, description, identifier_type,
        identifier_name, identifier_value, validThrough, hiring_organization_type,
        hiring_organization_name, hiring_organization_logo, hiring_organization_same_as
		
	) VALUES (
		$1, $2, $3, $4, $5, $6, $7, $8, $9, $10,
		$11, $12, $13, $14
	)`
)

func (s *SelenuimStoragePostgres) Create(name string) error {
	list := selenium.SeleniumPArseFireFox(name) //list
	////Для тестов запись в файл
	//for _, text := range list {
	//	f, _ := os.OpenFile("test.json", os.O_APPEND|os.O_WRONLY, 0600)
	//	file, _ := json.Marshal(text)
	//	f.Write(file)
	//	f.WriteString("\n")
	//}
	//byte1, _ := os.ReadFile("test.json")
	////	var list []models.Vacancy
	//_ = json.Unmarshal(byte1, list)
	//fmt.Println(list)

	// Чтение файла
	//file, _ := os.Open("test.json")
	//defer file.Close()
	//var list []models.Vacancy
	//
	//byteValue, _ := os.ReadFile(file.Name())
	//_ = json.Unmarshal(byteValue, &list)
	count := 1
	if len(list) > 0 {
		for i := range list {
			if list[i].Type == "" {
				continue
			}
			_, err := s.db.Exec(TablePostgres, count, list[i].Context, list[i].Type, list[i].DatePosted, list[i].Title,
				list[i].Description, list[i].Identifier.Type, list[i].Identifier.Name, list[i].Identifier.Value,
				list[i].ValidThrough, list[i].HiringOrganization.Type, list[i].HiringOrganization.Name,
				list[i].HiringOrganization.Logo, list[i].HiringOrganization.SameAs)

			count++
			if err != nil {
				log.Println(err)
			}
		}
		return nil
	}
	return errors.New("not create")
}

func (s *SelenuimStoragePostgres) GetById(id int) (models.Vacancy, error) {
	var vacancy models.Vacancy
	if s.db != nil {
		sqlStatment := `SELECT id, context, type, datePosted, tittle, description, identifier_type,
        identifier_name, identifier_value, validThrough, hiring_organization_type,
        hiring_organization_name, hiring_organization_logo, hiring_organization_same_as FROM vacancy WHERE id=$1`
		row := s.db.QueryRow(sqlStatment, id)
		err := row.Scan(&vacancy.Id, &vacancy.Context, &vacancy.Type, &vacancy.DatePosted, &vacancy.Title,
			&vacancy.Description, &vacancy.Identifier.Type, &vacancy.Identifier.Name, &vacancy.Identifier.Value,
			&vacancy.ValidThrough, &vacancy.HiringOrganization.Type, &vacancy.HiringOrganization.Name, &vacancy.HiringOrganization.SameAs,
			&vacancy.HiringOrganization.Logo)
		if err != nil {
			log.Fatal(err)
			return models.Vacancy{}, err
		}
		return vacancy, nil
	}
	return models.Vacancy{}, errors.New("db is empty")
}

func (s *SelenuimStoragePostgres) GetList() ([]models.Vacancy, error) {

	var vacansies []models.Vacancy
	if s.db != nil {
		sqlStatment := `SELECT id, context, type, datePosted, tittle, description, identifier_type,
        identifier_name, identifier_value, validThrough, hiring_organization_type,
        hiring_organization_name, hiring_organization_logo, hiring_organization_same_as FROM vacancy`
		row, err := s.db.Query(sqlStatment)
		if err != nil {
			log.Fatalf("Unable to execute the query. %v", err)
		}
		defer row.Close()
		for row.Next() {
			var vacancy models.Vacancy
			err := row.Scan(&vacancy.Id, &vacancy.Context, &vacancy.Type, &vacancy.DatePosted, &vacancy.Title,
				&vacancy.Description, &vacancy.Identifier.Type, &vacancy.Identifier.Name, &vacancy.Identifier.Value,
				&vacancy.ValidThrough, &vacancy.HiringOrganization.Type, &vacancy.HiringOrganization.Name, &vacancy.HiringOrganization.SameAs,
				&vacancy.HiringOrganization.Logo)
			if err != nil {
				log.Fatalf("Unable to scan the row. %v", err)
			}
			vacansies = append(vacansies, vacancy)
		}
		return vacansies, nil
	}
	return []models.Vacancy{}, errors.New("db is empty")
}

func (s *SelenuimStoragePostgres) Delete(id int) error {
	if s.db != nil {
		sqlStatment := `DELETE FROM vacancy WHERE id=$1`
		_, err := s.db.Exec(sqlStatment, id)
		if err != nil {
			log.Fatal(err)
		}
		return nil
	}
	return errors.New("db is empty")
}
