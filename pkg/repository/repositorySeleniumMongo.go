package repository

import (
	"context"
	"errors"
	"fmt"
	"gitlab.com/a3buka/BD/internal/models"
	"gitlab.com/a3buka/BD/pkg/selenium"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"log"
	"strconv"
)

type repositorySeleniumMongo struct {
	client *mongo.Collection
}

func NewRepositorySeleniumMongo(client *mongo.Collection) *repositorySeleniumMongo {
	return &repositorySeleniumMongo{client: client}
}

func (r *repositorySeleniumMongo) Create(name string) error {
	list := selenium.SeleniumPArseFireFox(name) //list
	// оставлю для теста
	//file, _ := os.Open("test.json")
	//defer file.Close()
	//var list []models.Vacancy
	//
	//byteValue, _ := os.ReadFile(file.Name())
	//_ = json.Unmarshal(byteValue, &list)
	count := 1
	if len(list) > 0 {
		for i := range list {
			if list[i].Type == "" {
				continue
			}
			vacancy := bson.M{
				"id":          strconv.Itoa(count),
				"context":     list[i].Context,
				"type":        list[i].Type,
				"datePosted":  list[i].DatePosted,
				"title":       list[i].Title,
				"description": list[i].Description,
				"identifier": bson.M{
					"type":  list[i].Identifier.Type,
					"name":  list[i].Identifier.Name,
					"value": list[i].Identifier.Value,
				},
				"validThrough": list[i].ValidThrough,
				"hiringOrganization": bson.M{
					"type":   list[i].HiringOrganization.Type,
					"name":   list[i].HiringOrganization.Name,
					"logo":   list[i].HiringOrganization.Logo,
					"sameAs": list[i].HiringOrganization.SameAs,
				},
			}

			res, err := r.client.InsertOne(context.TODO(), vacancy)
			count++
			if err != nil {
				log.Println(err)
			}
			fmt.Println(res.InsertedID)
		}
		return nil
	}
	return errors.New("not create")
}

func (r *repositorySeleniumMongo) GetById(id int) (models.Vacancy, error) {
	var vacansy = models.Vacancy{}
	err := r.client.FindOne(context.TODO(), bson.M{"id": strconv.Itoa(id)}).Decode(&vacansy)
	if err != nil {
		return models.Vacancy{}, err
	}
	return vacansy, nil
}

func (r *repositorySeleniumMongo) GetList() ([]models.Vacancy, error) {
	cursor, err := r.client.Find(context.TODO(), bson.M{})
	if err != nil {
		log.Fatal(err)
	}
	var vacancies []models.Vacancy

	defer cursor.Close(context.TODO())
	for cursor.Next(context.TODO()) {
		var vacancy models.Vacancy

		if err = cursor.Decode(&vacancy); err != nil {
			return nil, err
		}
		vacancies = append(vacancies, vacancy)
	}
	return vacancies, nil
}

func (r *repositorySeleniumMongo) Delete(id int) error {
	if _, err := r.client.DeleteOne(context.TODO(), bson.M{"id": strconv.Itoa(id)}); err != nil {
		return err
	}
	return nil
}
