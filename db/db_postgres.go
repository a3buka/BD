package db

import (
	"context"
	"fmt"
	"github.com/jmoiron/sqlx"
	"github.com/joho/godotenv"
	_ "github.com/lib/pq"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"log"
	"os"
	"time"
)

const postgresTableCreate = `CREATE TABLE IF NOT EXISTS vacancy (
    id 						     serial ,
    context                     varchar,
    type                        varchar,
    datePosted                  varchar,
    tittle                      varchar,
    description                 varchar,
    identifier_type				varchar,
    identifier_name		 		varchar,
    identifier_value			varchar,                             
    validThrough                varchar,
    hiring_organization_type    varchar,
    hiring_organization_name    varchar,
    hiring_organization_logo    varchar,
    hiring_organization_same_as varchar                  
                                     )`

func NewPostgresDB() (*sqlx.DB, *mongo.Collection, error) {
	err := godotenv.Load(".env")
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	switch os.Getenv("DB") {
	case "POSTGRES":
		{
			db, err := sqlx.Open("postgres", fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s sslmode=%s",
				os.Getenv("DB_HOST"), os.Getenv("DB_PORT"),
				os.Getenv("DB_USER"), os.Getenv("DB_NAME"),
				os.Getenv("DB_PASSWORD"), "disable"))
			if err != nil {
				log.Fatalln(err)
			}
			err = db.Ping()
			if err != nil {
				log.Fatalln(err)
			}
			_, err = db.Exec(postgresTableCreate)
			if err != nil {
				log.Fatalln(err)
			}

			return db, nil, nil
		}
	case "MONGO":
		{
			//путь
			var mongoDBURL string

			mongoDBURL = fmt.Sprintf("mongodb://%s:%s",
				os.Getenv("MONGO_HOST"), os.Getenv("MONGO_PORT"))
			client, err := mongo.NewClient(options.Client().ApplyURI(mongoDBURL))

			ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)

			err = client.Connect(ctx)
			if err != nil {
				return nil, nil, err
			}
			//проверка
			databases, _ := client.ListDatabaseNames(ctx, bson.M{})
			fmt.Println(databases)
			err = client.Ping(ctx, readpref.Primary())
			if err != nil {
				panic(err)
				return nil, nil, err
			}
			fmt.Println("Connected to MongoDB!")

			collection := client.Database("kata").Collection("vacancy")
			return nil, collection, nil
		}
	}
	return nil, nil, err
}
