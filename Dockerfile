FROM golang:latest

COPY . .

ENV GOPATH=/

RUN go mod download

RUN go build -o main .

CMD ["./main"]





