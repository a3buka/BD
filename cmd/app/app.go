package app

import (
	"context"
	"errors"
	"gitlab.com/a3buka/BD/db"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/a3buka/BD/internal/server"

	"gitlab.com/a3buka/BD/pkg/handler"

	"github.com/sirupsen/logrus"
	"gitlab.com/a3buka/BD/pkg/repository"
	"gitlab.com/a3buka/BD/service"
)

func RunSelenium() {
	DB, client, err := db.NewPostgresDB()
	if err != nil {
		logrus.Errorf("DB is %s: ", err.Error())
	}

	repository := repository.NewRepositorySelenium(DB, client)
	service := service.NewVacansyService(repository)
	handler := handler.NewHandler(service)
	srv := server.NewServer(handler.InitRoutes())

	go func() {
		if err := srv.Run(); !errors.Is(err, http.ErrServerClosed) {
			logrus.Errorf("server it's not worked %s", err.Error())
		}
	}()

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGTERM, syscall.SIGINT)
	<-quit

	logrus.Print("TodoApp Shutting Down")

	if err := srv.Shutdown(context.Background()); err != nil {
		logrus.Errorf("error occured on server shutting down: %s", err.Error())
	}

}
